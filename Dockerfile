FROM reg.csphere.cn/tomcat:7.0-jre7

MAINTAINER richie.hao <hxq@nicescale.com>

WORKDIR /usr/local/tomcat/webapps/
COPY target/agilefant.war /usr/local/tomcat/webapps/
COPY init.sh /data/init.sh
#ADD target/agilefant.war /usr/local/tomcat/webapps/
RUN unzip /usr/local/tomcat/webapps/agilefant.war -d agilefant && rm /usr/local/tomcat/webapps/agilefant.war

VOLUME ["/usr/local/tomcat/webapps"]

WORKDIR $CATALINA_HOME

EXPOSE 8080
CMD ["/data/init.sh","catalina.sh", "run"] 
